﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <conio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

int main()
{
	setlocale(LC_ALL, "rus");
	SetConsoleCP(1251); SetConsoleOutputCP(1251);
	int n, n1, r, k;
	char str1[15];
	srand(time(0));
	char *str3 = (char*)calloc(100, sizeof(char));
	printf(" Введите строку:\n (в конце строки нужно ставить . через пробел)\n");
	do
	{
		n1 = scanf("%s", str1);
		if (*str1 == '.')
		{
			break;
		}
		n = strlen(str1);
		bool *e = (bool*)calloc(n, sizeof(bool));
		char *str2 = (char*)calloc(n + 2, sizeof(char));
		for (int i = 0; i < n; i++)
		{
			r = rand() % n;
			if (!e[r])
			{
				str2[i] = str1[r];
				e[r] = true;
			}
			else
			{
				i--;
			}
		}
		str2[n] = ' ';
		strcat(str3, str2);
		free(e); free(str2);
	} while (n1 != 0);
	printf("\n");
	printf(" Строка которую переместили:\n");
	n1 = puts(str3);
	free(str3);
	_getch();
	return 0;
}